/* **************** Define Steppers libs and vars **************** */
#include <Stepper.h> // Include the header file
#define STEPS 32

Stepper stepper(STEPS, 8, 10, 9, 11);

/* **************** Define Sleepmode libs and vars **************** */
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

uint8_t sleep_period = 1;

/* **************** Define RTC libs and vars **************** */
#include <Wire.h>
#include "ds3231.h"
#define BUFF_MAX 256


/* **************** Define var for script **************** */
const int         interruptPin = 0;
volatile boolean  change       = false;

int               circle       = 2048;
int               nbCircle     = 2;


//                            J1  J2  F1  F2  M1  M2  A1  A2  M1  M2  J1  J2
//                            D1  D2  N1  N2  O1  O2  S1  S2  A1  A2  J1  J2
int LeveSoleilHeure[12]=    { 07, 06, 06, 06, 05, 05, 04, 04, 04, 04, 04, 04 };  
int LeveSoleilMinute[12]=   { 00, 50, 40, 20, 40, 00, 30, 05, 05, 00, 00, 00 };  
int CoucheSoleilHeure[12]=  { 18, 18, 18, 19, 19, 20, 20, 20, 20, 20, 21, 21 };  
int CoucheSoleilMinute[12]= { 10, 15, 30, 00, 45, 35, 00, 25, 45, 55, 15, 35 };  



/* ***************************************************
   *                 Functions                       *
   *************************************************** */

/**
 * This function is used to debug by showing the date
 */
void printDate(ts t){
  Serial.print("date : ");
  Serial.print(t.mday);
  Serial.print("/");
  Serial.print(t.mon);
  Serial.print("/");
  Serial.print(t.year);
  Serial.print("\t Heure : ");
  Serial.print(t.hour);
  Serial.print(":");
  Serial.print(t.min);
  Serial.print(".");
  Serial.println(t.sec);
 
  delay(500);
}

boolean isSunrise(int tabIndex, ts t){
  uint8_t tmpH = t.hour;
  uint8_t tmpM;
  if(tmpH < LeveSoleilHeure[tabIndex]){
    return true;
  }
  else if(LeveSoleilHeure[tabIndex] < tmpH && tmpH < CoucheSoleilHeure[tabIndex]){
    return false;
  }
  else if(tmpH > CoucheSoleilHeure[tabIndex]){
    return true;
  }
  else if(tmpH == LeveSoleilHeure[tabIndex]){
    tmpM = t.min;
    if(tmpM <= LeveSoleilMinute[tabIndex]){
      return true;
    }else{
      return false;
    }
  }
  else{
    // tmpH == LeveSoleilHeure[tabIndex]
    tmpM = t.min;
    if(tmpM <= CoucheSoleilMinute[tabIndex]){
      return false;
    }else{
      return true;
    }
  }
}

boolean isMorning(ts t){
  return t.hour < 12;
}

int getIndexForDate(ts t){
  int tabIndex = t.mon;   
  if (t.mon > 6)   
  {  
    tabIndex = 13 - t.mon;    
    tabIndex = (tabIndex - 1) * 2;     
    if( t.mday < 15)   
      tabIndex++;   
  }  
  else   
  {  
    tabIndex = (tabIndex - 1) * 2;     
    if( t.mday > 15)   
      tabIndex++;   
  }

  return tabIndex;
}

/**
 * This function is used to set the next alarm, according to the current date and the next 
 */
void set_next_alarm(void)
{
    Serial.println("Set next alarm");

    struct ts t;
    DS3231_get(&t);

    int tabIndex    = getIndexForDate(t);
    boolean sunrise = isSunrise(tabIndex, t);

    int h, m;

    //If it is morning, we have to set the sunset
    if(sunrise){
      h = LeveSoleilHeure[tabIndex];
      m = LeveSoleilMinute[tabIndex];
    }else{
      h = CoucheSoleilHeure[tabIndex];
      m = CoucheSoleilMinute[tabIndex];
    }

    printDate(t);
    Serial.print("Alarme à : ");
    Serial.print(h);
    Serial.print(":");
    Serial.println(m);

    // flags define what calendar component to be checked against the current time in order
    // to trigger the alarm
    // A2M2 (minutes) (0 to enable, 1 to disable)
    // A2M3 (hour)    (0 to enable, 1 to disable) 
    // A2M4 (day)     (0 to enable, 1 to disable)
    // DY/DT          (dayofweek == 1/dayofmonth == 0)
    uint8_t flags[4] = { 0, 0, 1, 1 };

    // set Alarm2. only the minute is set since we ignore the hour and day component
    DS3231_clear_a2f();
    DS3231_set_a2(m, h, 0, flags);

    // activate Alarm2
    DS3231_set_creg(DS3231_CONTROL_INTCN | DS3231_CONTROL_A2IE);
    delay(500);
}

/**
 * This function is calling by the interupt process. Only change var's values
 */
void upOrDown() {
  change = true;
}

/**
 * This function is used to enter in sleep mode
 */
void enterSleep(void)
{
  set_sleep_mode(SLEEP_MODE_ADC);
  sleep_enable();

  // Now enter sleep mode.
  Serial.println("Dodo");
  sleep_mode();
  
  // The program will start HERE after wake up
  sleep_disable();
  Serial.println("Wake Up !");
  
  // Re-enable the peripherals.
  power_all_enable();

  // Debug
  Serial.println("All is ok, time to loop");
}



/* **********************************************************
   *                 Script Functions                       *
   ******************************************************** */
void setup()
{
    //Init Serial
    Serial.begin(9600);
    
    //Init Stepper
    stepper.setSpeed(200);

    //Init Sleep mode
    pinMode(interruptPin, INPUT_PULLUP);
    attachInterrupt(interruptPin, upOrDown, CHANGE);
    
    //Init RTC
    Wire.begin();
    DS3231_init(DS3231_CONTROL_INTCN);
    DS3231_clear_a2f();

    //Set the first alarm
    set_next_alarm();
}

void loop()
{
    Serial.println("Loop");

    //If RTC wakup the Arduino card, the var "change" is up to "True" and the stepper has to be activate
    if(change){
      Serial.println("Dans le if");
      
      // To up => -1
      // To down => 1
      struct ts t;
      DS3231_get(&t);
      int way = isMorning(t) ? -1 : 1;
      int addIfUp = way == -1 ? 180 : 0;
      
      stepper.step(way*(nbCircle*circle + addIfUp));
      Serial.println(way*(nbCircle*circle + addIfUp));
      change = false;
    }

    //Then we change the alarm
    set_next_alarm();

    //And going to sleep
    Serial.println("Going to coucher");
    enterSleep();

    //This delay is there to give Serial time to print the texts
    delay(50);
}
